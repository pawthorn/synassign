
SASSDB = {
    ['zones'] = {},

    ['last_page'] = {},-- {zone, wing, page}
}

local MaxRows = 6
local MaxColumns = 8 -- All raid marks & unmarked
local IconTranslations = {
    "Skull",
    "X",
    "Square",
    "Moon",
    "Triangle",
    "Diamond",
    "Circle",
    "Star",
    "Unmarked"
}
local IconSendTags = {
    ["Skull"] = "{skull}",
    ["X"] = "{cross}",
    ["Square"] = "{square}",
    ["Moon"] = "{moon}",
    ["Triangle"] = "{triangle}",
    ["Diamond"] = "{diamond}",
    ["Circle"] = "{circle}",
    ["Star"] = "{star}",
    ["Unmarked"] = "Unmarked"
}
local InvalidKeys = {
    ['tactics'] = true,
    ['LastPage'] = true,
}

local Cleaning = false
local Icons = {}
local SendBoxes = {}
local RoleBoxes = {}
local MobBoxes = {}
local NameBoxes = {}
local PasteButton
local TacticsBox
local DEFAULT_OBJECT = {
    ['wings'] = {},
    ['pages'] = {},
    ['zones'] = {},
}
local selectedGroup


synassign = LibStub("AceAddon-3.0"):NewAddon("synassign", "AceComm-3.0", "AceEvent-3.0", "AceSerializer-3.0")
synassign.COMM_PREFIX = 'SYNASS'
local libCompress = LibStub("LibCompress")
local libCompressET = libCompress:GetAddonEncodeTable()
local AceGUI = LibStub("AceGUI-3.0")

-- Common functions
local function strTok(word, ch)
  local toks = {}
  local i
  local j = 0
  word = word or ''
  while j ~= nil do
    local start = j and j + 1 or 0
    i, j = word:find(ch, start)
    if i == nil then
      i = #word + 1
    end
    local str = word:sub(start, i - 1)
    if str then
        table.insert(toks, str)
    end
  end
  return toks
end

local function strStartswith(str, token)
    return str:sub(1, token:len()) == token
end

local function strTrim(str)
   local n = str:find"%S"
   return n and str:match(".*%S", n) or ""
end

local function strValid(s)
    return s and s ~= ''
end

local function strpairs(t)
    local keys = {}
    local kv = {}
    for k in pairs(t) do
        local ku = (k or ''):upper()
        keys[#keys + 1] = ku
        kv[ku] = k
    end
    table.sort(keys)
    -- return the iterator function
    local i = 0
    return function()
        i = i + 1
        if keys[i] then
            return kv[keys[i]], t[kv[keys[i]]]
        end
    end
end

local function strConcat(...)
    local str = ''
    local n = select('#', ...)
    local args = {...}
    for i = 1, n do
        str = str .. tostring(args[i])
    end
    return str
end

local function copyTableWithout(obj, key)
    local ret = {}
    for k, v in pairs(obj) do
        if k ~= key then
            ret[k] = v
        end
    end
    return ret
end

-- https://web.archive.org/web/20131225070434/http://snippets.luacode.org/snippets/Deep_Comparison_of_Two_Values_3
local function deepcompare(t1,t2,ignore_mt)
    local ty1 = type(t1)
    local ty2 = type(t2)
    if ty1 ~= ty2 then return false end
    -- non-table types can be directly compared
    if ty1 ~= 'table' and ty2 ~= 'table' then return t1 == t2 end
    -- as well as tables which have the metamethod __eq
    local mt = getmetatable(t1)
    if not ignore_mt and mt and mt.__eq then return t1 == t2 end
    for k1,v1 in pairs(t1) do
        local v2 = t2[k1]
        if v2 == nil or not deepcompare(v1,v2) then return false end
    end
    for k2,v2 in pairs(t2) do
        local v1 = t1[k2]
        if v1 == nil or not deepcompare(v1,v2) then return false end
    end
    return true
end

local function canSendSync(player)
    local isLeader = UnitIsGroupLeader(player)
    local isAssistant = UnitIsGroupAssistant(player)
    return isLeader or (isAssistant and isAssistant ~= player)
end

-- Read DB
local function getSelectedPage()
    local zone, wing, page = unpack(strTok(SASSDB.last_page, '\001'))
    return ((SASSDB.zones[zone] or DEFAULT_OBJECT).wings[wing] or DEFAULT_OBJECT).pages[page]
end

-- Tree Group generation/population
local function generateTree()
    local tree = {}

    for zname, zone in strpairs(SASSDB.zones) do
        local zoneItem = {
            value = zname,
            text = zname,
            children = {}
        }
        zone.name = zname

        for wname, wing in strpairs(zone.wings) do
            local wingItem = {
                value = wname,
                text = wname,
                children = {}
            }
            wing.name = wname

            for pname, page in strpairs(wing.pages) do
                local pageItem = {
                    value = pname,
                    text = pname,
                }
                page.name = pname
                if not page.data[1] then
                    page.data[1] = {}
                end
                page.data[1][1] = pname

                table.insert(wingItem["children"], pageItem)
            end
            table.insert(zoneItem["children"], wingItem)
        end
        table.insert(tree, zoneItem)
    end
    table.sort(tree, function (a, b) return a.text < b.text end)
    return tree
end

local function generateMembers()
    local members = {}
    for i = 1, 40 do
        local name, _, _, _, _, cls, _, _, _, role = GetRaidRosterInfo(i)
        if name ~= nil then
            table.insert(members, {
                ['name'] = name,
                ['class'] = cls,
                ['role'] = role,
            })
        end
    end
    return members
end

local function raid_members(members)
    local names = {''}
    for _, mem in ipairs(members) do
        table.insert(names, mem.name)
    end
    table.sort(names)
    return names
end

local function class_members(...)
    local matches = {}
    local args = {...}
    for i = 1, #args do
        matches[args[i]] = true
    end
    return function(members)
        local names = {''}
        for _, mem in ipairs(members) do
            if matches[mem.class] then
                table.insert(names, mem.name)
            end
        end
        table.sort(names)
        return names
    end
end

local function tank_members(members)
    local names = {''}
    for _, mem in ipairs(members) do
        if mem.role == 'MAINTANK' then
            table.insert(names, mem.name)
        end
    end
    if #names <= 0 then
        names = raid_members()
    end
    table.sort(names)
    return names
end

local function assist_members(members)
    local names = {''}
    for _, mem in ipairs(members) do
        if canSendSync(mem.name) then
            table.insert(names, mem.name)
        end
    end
    if #names <= 0 then
        names = raid_members()
    end
    table.sort(names)
    return names
end

local ROLE_MAPPINGS = {
    ['TANK'] = tank_members,
    ['MAINTANK'] = tank_members,
    ['ASSIST'] = assist_members,
    ['MARK'] = assist_members,
    ['GROUPS'] = assist_members,
    ['HEAL'] = class_members('DRUID', 'PRIEST', 'PALADIN', 'SHAMAN'),
    ['*'] = raid_members,

    ['DRUID'] = class_members('DRUID'),
    ['HUNTER'] = class_members('HUNTER'),
    ['MAGE'] = class_members('MAGE'),
    ['PALADIN'] = class_members('PALADIN'),
    ['PRIEST'] = class_members('PRIEST'),
    ['ROGUE'] = class_members('ROGUE'),
    ['SHAMAN'] = class_members('SHAMAN'),
    ['WARLOCK'] = class_members('WARLOCK'),
    ['WARRIOR'] = class_members('WARRIOR'),

    ['SLEEP'] = class_members('DRUID'),
    ['HOTS'] = class_members('DRUID', 'PRIEST'),
    ['TRAP'] = class_members('HUNTER'),
    ['PACK'] = class_members('HUNTER'),
    ['KITE'] = class_members('HUNTER', 'MAGE'),
    ['POLY'] = class_members('MAGE'),
    ['SHEEP'] = class_members('MAGE'),
    ['SHACKLE'] = class_members('PRIEST'),
    ['MC'] = class_members('PRIEST'),
    ['CONTROL'] = class_members('PRIEST'),
    ['STUN'] = class_members('ROGUE', 'PALADIN'),
    ['KICK'] = class_members('ROGUE', 'WARRIOR', 'MAGE'),
    ['INTERRUPT'] = class_members('ROGUE', 'WARRIOR', 'MAGE'),
    ['TREMOR'] = class_members('SHAMAN', 'PRIEST'),
    ['WARD'] = class_members('SHAMAN', 'PRIEST'),
    ['FEARWARD'] = class_members('SHAMAN', 'PRIEST'),
    ['FEAR WARD'] = class_members('SHAMAN', 'PRIEST'),
    ['FEAR'] = class_members('PRIEST', 'WARLOCK', 'WARRIOR'),
    ['HOWL'] = class_members('WARRIOR'),

    ['DECURSE'] = class_members('DRUID', 'MAGE'),
    ['DISEASE'] = class_members('PALADIN', 'PRIEST', 'SHAMAN'),
    ['DEDISEASE'] = class_members('PALADIN', 'PRIEST', 'SHAMAN'),
    ['DISPEL'] = class_members('PALADIN', 'PRIEST'),
    ['TRANQ'] = class_members('HUNTER'),
    ['POISON'] = class_members('SHAMAN', 'PALADIN', 'DRUID'),
    ['DEPOISON'] = class_members('SHAMAN', 'PALADIN', 'DRUID'),
}


local function populatePage(page)
    if page == nil then
        Cleaning = false
        return
    end
    local choices = {}
    local choiceMap = {}
    local members = generateMembers()
    if page.tactics then
        TacticsBox:SetText(page.tactics or "")
    end

    for j = 1, MaxColumns do
        local row = page.data[1] or {}
        if row[j + 1] and row[j + 1]:upper() ~= IconTranslations[j]:upper() then
            MobBoxes[j]:SetText(row[j + 1])
        end
    end

    for i, row in pairs(page.data) do
        i = tonumber(i)
        if i > 1 and row ~= nil and strValid(row[1]) then
            SendBoxes[i-1]:SetDisabled(false)
            local role = row[1]:upper()
            local mapping = '*'
            for k, _ in pairs(ROLE_MAPPINGS) do
                if strStartswith(role, k) then
                    mapping = k
                end
            end
            if choices[mapping] == nil then
                choices[mapping] = ROLE_MAPPINGS[mapping](members)
                if #choices[mapping] <= 1 then
                    choices[mapping] = ROLE_MAPPINGS['*'](members)
                end
                choiceMap[mapping] = {}
                for j, n in pairs(choices[mapping]) do
                    choiceMap[mapping][n] = j
                end
            end
            local ii = i - 1
            RoleBoxes[ii]:SetText(row[1])
            for j = 1, MaxColumns do
                NameBoxes[ii][j]:SetDisabled(false)
                NameBoxes[ii][j]:SetList({unpack(choices[mapping])})
                NameBoxes[ii][j].choices = choices[mapping]
                if row[j + 1] then
                    local boxchoice = choiceMap[mapping][row[j + 1]]
                    if boxchoice == nil then
                        NameBoxes[ii][j].text:SetTextColor(1,0.5,0.5)
                        NameBoxes[ii][j]:SetText('??? ' .. row[j + 1])
                    else
                        NameBoxes[ii][j]:SetValue(boxchoice)
                    end
                end
            end
        end
    end
    Cleaning = false
end

local function cleanPage()
    Cleaning = true
    for row = 1, MaxRows do
        RoleBoxes[row]:SetText("")
        SendBoxes[row]:SetDisabled(true)
    end

    for col = 1, MaxColumns do
        -- Icons[col]:SetLabel("-")
        MobBoxes[col]:SetText("")
        for row = 1, MaxRows do
            NameBoxes[row][col]:SetDisabled(true)
            NameBoxes[row][col]:SetText("")
            NameBoxes[row][col]:SetValue(0)
        end
    end

    PasteButton:SetDisabled(synassign.COPIED_PAGE == nil)

    TacticsBox:SetText("")
end

function synassign:PasteValues()
    local zone, wing, pname = unpack(strTok(synassign.COPIED_PAGE, '\001'))
    local other = SASSDB.zones[zone].wings[wing].pages[pname]
    local page = getSelectedPage()

    local lines = {}
    for i = 2, MaxRows +1 do
        if other.data[i] and strValid(other.data[i][1]) then
            lines[other.data[i][1]] = other.data[i]
        end
    end
    for i = 2, MaxRows +1 do
        if page.data[i] and lines[page.data[i][1]] ~= nil then
            local ol = lines[page.data[i][1]]
            for j, x in ipairs(ol) do
                if strValid(x) then
                    page.data[i][j] = x
                end
            end
        end
    end

    cleanPage()
    populatePage(getSelectedPage())
end


function synassign:setPage(page, force)
    if force == nil and SASSDB.last_page == page then
        return
    end

    local tbl = strTok(page, "\001")

    if tbl[3] then
        SASSDB.last_page = page
        self.treeGroup:SelectByValue(page)
        self.f:SetTitle(strConcat(tbl[1], '/',tbl[2], '/',tbl[3]))

        cleanPage()
        populatePage(getSelectedPage())
    end
end

local function editBoxTextChanged(self, event, text)
    if Cleaning then return end
    local page = getSelectedPage()
    if not page.data[self.row] then
        page.data[self.row] = {}
    end
    page.data[self.row][self.col] = text
end

local function tacticsTextChanged(self, event, text)
    if Cleaning then return end
    local page = getSelectedPage()
    page.tactics = self:GetText()
end

local function roleBoxTextChanged(self, event, _)
    if Cleaning then return end
    local page = getSelectedPage()
    local pos = self.editbox:GetCursorPosition()
    if not page.data[self.row] then
        page.data[self.row] = {}
    end
    local text = self:GetText()
    page.data[self.row][1] = text

    cleanPage()
    populatePage(getSelectedPage())
    self.editbox:SetCursorPosition(pos)
end

local function dropDownTextChanged(self, event, key)
    if Cleaning then return end
    local page = getSelectedPage()
    local text = self:GetText()
    if text == '' then
        text = nil
    end
    page.data[self.row][self.col] = text
    self.text:SetTextColor(1,1,1)
end

local function sendMessage(...)
    local message = strConcat(...)
    message = strTrim(message)
    if message == nil or message == "" then
        return
    end

    local tp
    if IsInRaid() then
        tp = "RAID"
    else
        tp = "PARTY"
    end

    for _, s in pairs(strTok(message, "\n")) do
        SendChatMessage(s, tp)
    end
end

local function sendRaidWarning(...)
    local message = strConcat(...)
    if IsInRaid() and canSendSync(UnitName("player")) then
        SendChatMessage(message, "RAID_WARNING")
    else
        sendMessage(message)
    end
end

local function sendRow(row)
    local page = getSelectedPage()
    local role = page.data[row][1]
    if not role then
        return
    end

    sendRaidWarning(role:upper() .. " ASSIGNMENTS IN RAID CHAT")
    for col = 2, MaxColumns +1 do
        local text = page.data[row][col]
        if strValid(text) then
            local tag = IconSendTags[IconTranslations[col -1]]
            local msg = tostring(tag)
            if page.data[1][col] then
                msg = msg .. ' (' .. page.data[1][col] .. ')'
            end
            msg = msg .. ": " .. tostring(text)
            sendMessage(msg)
        end
    end
end

local function sendColumn(col, event, to)
    local tag = IconSendTags[IconTranslations[col-1]]
    local page = getSelectedPage()
    local mobText = page.data[1][col] or IconTranslations[col-1]
    local out = tag .. " " .. mobText
    sendRaidWarning(out:upper() .. " ASSIGNMENTS IN RAID CHAT")
    -- sendMessage(out)

    for row = 2, MaxRows +1 do
        local role = page.data[row] and page.data[row][1] or nil
        local text = page.data[row] and page.data[row][col] or nil
        if strValid(role) and strValid(text) then
            out = role:upper() .. ": " .. text
            sendMessage(out)
        end
    end
end

local function ChatHook(self, event, msg, author, ...)
    if canSendSync(UnitName("player")) then
        local name, _ = unpack(strTok(author, '-'))
        if strStartswith(msg:upper(), "!ROLE") then
            local page = getSelectedPage()
            local hadRole = false
            for row = 2, MaxRows +1 do
                for col = 2, MaxColumns +1 do
                    if page.data[row] and page.data[row][col] then
                        local field = page.data[row][col]
                        if field:match(name) then
                            local text = strConcat(
                                'SynAssign: ',
                                (strValid(page.data[row][1]) and page.data[row][1] or IconTranslations[col-1]),
                                ' ',
                                page.data[1][col],
                                ': ',
                                page.data[row][col]
                            )
                            SendChatMessage(text, 'WHISPER', nil, name)
                            hadRole = true
                        end
                    end
                end
            end
            if not hadRole then
                local text = 'SynAssign: no roles assigned for you'
                SendChatMessage(text, 'WHISPER', nil, name)
            end
        end
    end
end

function synassign:SendSync(path)
    if not canSendSync(UnitName("player")) then
        if IsInRaid() then
            self.f:SetStatusText("You must be a raid leader or assistant to send syncs.")
        else
            self.f:SetStatusText("You must be the party leader to send syncs.")
        end
        return
    end

    if not path then
        return
    end

    local zone, wing, page = unpack(strTok(path, '\001'))
    if not zone then
        return
    end

    local tp = "PARTY"
    if IsInRaid() then
        tp = "RAID"
    end
    self.f:SetStatusText("Syncing to " .. tp:lower() .. "...")

    local message = {
        path = path,
        zones = nil
    }
    local obj
    if wing == nil then
        obj = SASSDB.zones[zone]
    elseif page == nil then
        obj = SASSDB.zones[zone].wings[wing]
    else
        obj = SASSDB.zones[zone].wings[wing].pages[page]
    end
    message.zones = synassign:BuildTree(path, obj)

    local messageSerialized = libCompressET:Encode(libCompress:Compress(self:Serialize(message)))
    synassign.last_serial = message
    synassign:SendAddonMessage(synassign.COMM_PREFIX, messageSerialized, tp)
end

function synassign:SendAddonMessage(prefix, text, tp)
    -- pulled from aceserializer-3 as classic just seems to drop packets, so im using timers to delay things
    -- https://us.forums.blizzard.com/en/wow/t/classic-sendaddonmessage-problems/620054
    local MSG_MULTI_FIRST = "\001"
    local MSG_MULTI_NEXT  = "\002"
    local MSG_MULTI_LAST  = "\003"

    local textlen = #text
    local maxtextlen = 250
    local chunk = strsub(text, 1, maxtextlen)
    local prio = 'NORMAL'

    if textlen <= maxtextlen then
        ChatThrottleLib:SendAddonMessage(prio, prefix, chunk, tp)
    else
        chunk = strsub(text, 1, maxtextlen)
        ChatThrottleLib:SendAddonMessage(prio, prefix, MSG_MULTI_FIRST..chunk, tp)
        local pos = 1+maxtextlen
        local function sendChunks()
            if pos+maxtextlen <= textlen then
                chunk = strsub(text, pos, pos+maxtextlen-1)
                ChatThrottleLib:SendAddonMessage(prio, prefix, MSG_MULTI_NEXT..chunk, tp)
                pos = pos + maxtextlen
                self.f:SetStatusText(strConcat("Syncing to ", tp:lower(), "... ",
                    math.ceil(pos/textlen*100), '%'))
                C_Timer.After(0.5, sendChunks)
            else
                chunk = strsub(text, pos)
                ChatThrottleLib:SendAddonMessage(prio, prefix, MSG_MULTI_LAST..chunk, tp)
                self.f:SetStatusText(strConcat("Syncing to ", tp:lower(), "... 100%"))
            end
        end
        C_Timer.After(0.5, sendChunks)
    end
    -- self:SendCommMessage(synassign.COMM_PREFIX, text, tp, nil, 'NORMAL')
end

function synassign:AssignTree(other, updated)
    for zname, zone in pairs(other) do
        if not SASSDB.zones[zname] then
            SASSDB.zones[zname] = {
                wings = {}
            }
        end
        for wname, wing in pairs(zone.wings) do
            if not SASSDB.zones[zname].wings[wname] then
                SASSDB.zones[zname].wings[wname] = {
                    pages = {}
                }
            end
            for pname, page in pairs(wing.pages) do
                if not deepcompare(page, SASSDB.zones[zname].wings[wname].pages[pname]) then
                    if updated then updated[strConcat(zname,'/',wname,'/',pname)] = 'added' end
                    if SASSDB.zones[zname].wings[wname].pages[pname] then
                        local path = strConcat(zname,'\001',wname,'\001',pname)
                        local old = synassign:BuildTree(
                            path,
                            SASSDB.zones[zname].wings[wname].pages[pname]
                        )
                        synassign:HistoryInsert(path, old)
                        if updated then
                            updated[strConcat(zname,'/',wname,'/',pname)] = 'updated'
                        end
                    end
                    SASSDB.zones[zname].wings[wname].pages[pname] = page
                end
            end
        end
    end
end

function synassign:OnCommReceived(prefix, message, distribution, sender)
    if prefix ~= synassign.COMM_PREFIX then return end
    if not canSendSync(sender) or (distribution ~= "PARTY" and distribution ~= "RAID") then
        return
    end

    if UnitName("player") == sender then
        self.f:SetStatusText("Synced successfully!")
        return
    end

    local decoded = libCompressET:Decode(message)
    local decompressed, err = libCompress:Decompress(decoded)
    -- local decompressed = message
    if not decompressed then
        print("Failed to decompress sync: " .. err)
        return
    end

    local didDeserialize, obj = self:Deserialize(decompressed)
    if not didDeserialize then
        print("Failed to deserialize sync: " .. obj)
        return
    end

    if not obj["path"] or not obj["zones"] then
        print("Failed to parse deserialized comm.")
        return
    end

    local status = "sync from " .. sender .. ': '
    local updates = {}

    self:AssignTree(obj.zones, updates)

    local i = 0
    for k, v in strpairs(updates) do
        if i >= 1 then
            status = status .. ', '
        end
        status = strConcat(status, v, ' "', k, '"')
        i = i + 1
    end
    if i <= 0 then
        status = status .. 'nothing new'
    end

    self.f:SetStatusText(strConcat("[", date("%H:%M:%S"), "] ", status))
    print(strConcat("synassign: ", status))

    self.tree = generateTree()
    self.treeGroup:SetTree(self.tree)
    self.treeGroup:RefreshTree()

    self:setPage(SASSDB.last_page, true)
end

function synassign:AcceptRename(path, newname)
    local zone, wing, page = unpack(strTok(path, '\001'))
    if wing == nil then
        SASSDB.zones[newname] = SASSDB.zones[zone]
        SASSDB.zones = copyTableWithout(SASSDB.zones, zone)
    elseif page == nil then
        SASSDB.zones[zone].wings[newname] = SASSDB.zones[zone].wings[wing]
        SASSDB.zones[zone].wings = copyTableWithout(SASSDB.zones[zone].wings, wing)
    else
        SASSDB.zones[zone].wings[wing].pages[newname] = SASSDB.zones[zone].wings[wing].pages[page]
        SASSDB.zones[zone].wings[wing].pages =
            copyTableWithout(SASSDB.zones[zone].wings[wing].pages, page)
    end

    synassign:RefreshTree()
end

function synassign:RefreshTree()
    self.tree = generateTree()
    self.treeGroup:SetTree(self.tree)
    self.treeGroup:RefreshTree()
end

function synassign:GeneratePageText()
    local text = ''

    local page = getSelectedPage()

    for col = 1, MaxColumns+1 do
        local tag = IconSendTags[IconTranslations[col-1]]
        local line = (tag or ''):lower()
        line = line .. (#line > 0 and ' ' or '') .. (page.data[1][col] or IconTranslations[col-1])
        local hasText = false
        for row = 2, MaxRows +1 do
            if page.data[row] and strValid(page.data[row][col]) and strValid(page.data[row][1]) then
                hasText = true
            end
        end
        if hasText then
            for row = 2, MaxRows +1 do
                local r = page.data[row] or {}
                if strValid(r[1]) then
                    line = line .. ' -- ' .. (r[col] or '')
                end
            end
            text = text .. line .. '\n'
        end
    end
    if #text > 0 then
        text = text .. '\n'
    end

    if page and strValid(page.tactics) then
        text = text .. tostring(page.tactics or '')
    end
    return text
end

function synassign:SyncExorsus()
    if not canSendSync(UnitName("player")) then
        if IsInRaid() then
            self.f:SetStatusText("You must be a raid leader or assistant to send syncs.")
        else
            self.f:SetStatusText("You must be the party leader to send syncs.")
        end
        return
    end

    local text = self:GeneratePageText()
    local indextosnd = tostring(GetTime())..tostring(math.random(1000,9999))
    local packets = {}

    while #text > 0 do
        local curr = text:sub(1, 180)
        text = text:sub(181)
        table.insert(packets, curr)
    end

    for _, pack in ipairs(packets) do
        self:SendCommMessage(
            'EXRTADD',
            strConcat('multiline\t', indextosnd,'\t', pack),
            'RAID',
            nil,
            'NORMAL'
        )
    end
    self:SendCommMessage(
        'EXRTADD',
        strConcat('multiline_add\t', indextosnd,'\t-\tsynassign'),
        'RAID',
        nil,
        'NORMAL'
    )
end

function synassign:SyncChat()
    if not canSendSync(UnitName("player")) then
        if IsInRaid() then
            self.f:SetStatusText("You must be a raid leader or assistant to send syncs.")
        else
            self.f:SetStatusText("You must be the party leader to send syncs.")
        end
        return
    end

    local text = self:GeneratePageText()

    sendMessage(text)
end

function synassign:PopulateInitialData()
    if not SASSDB.history then
        SASSDB.history = {}
    end
    for k, _ in pairs(SASSDB.zones) do
        if not InvalidKeys[k] then return end
    end
    SASSDB = {
        ['zones'] = {
            ['a'] = {
                ['wings'] = {
                    ['b'] = {
                        ['pages'] = {
                            ['c'] = {
                                ['tactics'] = '',
                                ['data'] = {},
                            }
                        }
                    }
                }
            }
        },
        ['last_page'] = nil
    }
end

function synassign:Toggle()
    if self.f:IsVisible() then
        self.f:Hide()
    else
        self:RefreshTree()
        self:setPage(SASSDB.last_page, true)
        self.f:Show()
    end
end

function synassign:BuildTree(path, obj)
    local zone, wing, page = unpack(strTok(path, "\001"))
    -- print(strConcat('BuildTree ', path))
    local ret
    if wing == nil then
        ret = {
            [zone] = obj
        }
    elseif page == nil then
        ret = {
            [zone] = {
                wings = {
                    [wing] = obj
                }
            }
        }
    else
        ret = {
            [zone] = {
                wings = {
                    [wing] = {
                        pages = {
                            [page] = obj,
                        }
                    }
                }
            }
        }
    end
    return ret
end

function synassign:HistoryInsert(path, tree)
    if #SASSDB.history > 100 then
        SASSDB.history = {unpack(SASSDB.history, 2, 100)}
    end
    local data = self:Serialize(tree)
    table.insert(SASSDB.history, {
        path = path,
        datetime = date("%Y-%m-%d %T"),
        epoch = GetTime(),
        data = data,
    })
end

local function deleteTreeButton()
    local zone, wing, page = unpack(strTok(selectedGroup, "\001"))
    local obj

    if wing == nil then
        obj = SASSDB.zones[zone]
        SASSDB.zones = copyTableWithout(SASSDB.zones, zone)
    elseif page == nil then
        obj = SASSDB.zones[zone].wings[wing]
        SASSDB.zones[zone].wings = copyTableWithout(SASSDB.zones[zone].wings, wing)
    else
        obj = SASSDB.zones[zone].wings[wing].pages[page]
        SASSDB.zones[zone].wings[wing].pages =
            copyTableWithout(SASSDB.zones[zone].wings[wing].pages, page)
    end

    local tree = synassign:BuildTree(selectedGroup, obj)
    synassign:HistoryInsert(selectedGroup, tree)

    if SASSDB.last_page:sub(1, selectedGroup:len()) == selectedGroup then
        SASSDB.last_page = ""
    end
    synassign:RefreshTree()
end

local function createTreeButton()
    local zone, wing, page = unpack(strTok(selectedGroup, '\001'))
    print(strConcat('create ', zone,'/',wing,'/',page))

    StaticPopupDialogs["SYNASSIGN_CREATE"] = {
        text = "What do you want the new name to be?",
        button1 = "Create",
        button2 = "Cancel",
        OnAccept = function(self)
            local text = self.editBox:GetText()
            if wing == nil then
                zone = text
                wing = 'wing'
                page = 'page'
            elseif page == nil then
                wing = text
                page = 'page'
            else
                page = text
            end
            if not SASSDB.zones[zone] then
                SASSDB.zones[zone] = {wings = {}}
            end
            if not SASSDB.zones[zone].wings[wing] then
                SASSDB.zones[zone].wings[wing] = {pages = {}}
            end
            SASSDB.zones[zone].wings[wing].pages[page] = {
                data = {},
                tactics = '',
            }

            synassign:RefreshTree()
        end,
        timeout = 0,
        whileDead = true,
        hideOnEscape = true,
        exclusive = true,
        hasEditBox = true,
        preferredIndex = 10,
    }
    StaticPopup_Show("SYNASSIGN_CREATE")
end

local function renameTreeButton()
    local sg = selectedGroup
    local bits = strTok(selectedGroup, '\001')
    local raid, wing, page = unpack(bits)

    StaticPopupDialogs["SYNASSIGN_RENAME"] = {
        text = "How do you want to rename it?",
        button1 = "Rename",
        button2 = "Cancel",
        OnAccept = function(self)
            synassign:AcceptRename(sg, self.editBox:GetText())
        end,
        OnShow = function(self)
            self.editBox:SetText(page or wing or raid)
        end,
        timeout = 0,
        whileDead = true,
        hideOnEscape = true,
        exclusive = true,
        hasEditBox = true,
        preferredIndex = 10,
    }
    StaticPopup_Show("SYNASSIGN_RENAME")
end

local function moveTreeButton()
    local zone, wing, page = unpack(strTok(selectedGroup, '\001'))
    if wing == nil or zone == nil then
        return
    end

    StaticPopupDialogs["SYNASSIGN_MOVE"] = {
        text = "Where do you want to move this?",
        button1 = "Move",
        button2 = "Cancel",
        OnShow = function(self)
            self.editBox:SetText(strConcat(zone,'/',wing))
        end,
        OnAccept = function(self)
            local text = self.editBox:GetText()
            local zz, ww = unpack(strTok(text, '/'))
            if not strValid(zz) or not strValid(ww) then
                return
            end
            local obj = SASSDB.zones[zone].wings[wing].pages[page]
            if SASSDB.zones[zz].wings[ww].pages[page] ~= nil then
                local newpath = strConcat(zz,'\001',ww,'\001',page)
                local tree = synassign:BuildTree(newpath, obj)
                synassign:HistoryInsert(selectedGroup, tree)
            end
            SASSDB.zones[zz].wings[ww].pages[page] = obj

            SASSDB.zones[zone].wings[wing].pages =
                copyTableWithout(SASSDB.zones[zone].wings[wing].pages, page)

            synassign:RefreshTree()
        end,
        timeout = 0,
        whileDead = true,
        hideOnEscape = true,
        exclusive = true,
        hasEditBox = true,
        preferredIndex = 10,
    }
    StaticPopup_Show("SYNASSIGN_MOVE")
end

function synassign:MarkTargets()
    local page = getSelectedPage()
    local marked = {}
    for j = 2, MaxColumns +1 do
        for i = 2, MaxRows +1 do
            if marked[j] == nil and page.data[i] and
                    strValid(page.data[i][1]) and strValid(page.data[i][j]) then
                marked[j] = page.data[i][j]
                SetRaidTarget(marked[j], 9 - (j -1))
            end
        end
    end
end

function synassign:BuildUI()
    self.f = AceGUI:Create("Frame")
    self.f:Hide()
    self.f:SetTitle("SynAssign")
    self.f:SetLayout("Flow")
    self.f:SetWidth(1280)
    self.f:SetHeight(420)
    _G["synassignFrame"] = self.f.frame
    table.insert(UISpecialFrames, "synassignFrame")

    local iconDataBroker = LibStub("LibDataBroker-1.1"):NewDataObject("saMinimap", {
        type = "data source",
        -- text = "SynAssign",
        label = "SynAssign",
        icon = "Interface\\TargetingFrame\\UI-RaidTargetingIcon_8",
        OnClick = function()
            synassign:Toggle()
        end,
        OnTooltipShow = function(tooltip)
            tooltip:SetText("SynAssign")
            tooltip:Show()
        end,
    })
    local minimapIcon = LibStub("LibDBIcon-1.0")
    minimapIcon:Register("saMinimap", iconDataBroker, {hide = false})
    minimapIcon:Show()

    self.tree = generateTree()
    self.treeGroup = AceGUI:Create("TreeGroup")
    self.treeGroup:SetFullWidth(true)
    self.treeGroup:SetFullHeight(true)
    self.treeGroup:SetAutoAdjustHeight(false)
    self.treeGroup:SetTreeWidth(200)
    self.treeGroup:SetTree(self.tree)

    local treeGroupDrowndownMenu = _G["synassignDropdownMenu"]:New()
    treeGroupDrowndownMenu:AddItem("Create", createTreeButton)
    treeGroupDrowndownMenu:AddItem("Delete", deleteTreeButton)
    treeGroupDrowndownMenu:AddItem("Move", moveTreeButton)
    treeGroupDrowndownMenu:AddItem("Rename", renameTreeButton)
    treeGroupDrowndownMenu:AddItem("Sync", function()
        self:SendSync(selectedGroup)
    end)
    self.treeGroup:SetCallback("OnClick", function(_, _, group)
        if GetMouseButtonClicked() == "RightButton" then
            selectedGroup = group
            treeGroupDrowndownMenu:Show()
        end
    end)

    self.treeGroup:SetCallback("OnGroupSelected", function(_, _, group)
        self:setPage(group)
    end)

    -- Encounter view setup
    -- Create icons
    for col = 1, MaxColumns do
        local icon = AceGUI:Create("Icon")
        icon:SetImageSize(24, 24)
        if col > 8 then
            icon:SetImage("Interface\\Icons\\INV_Misc_QuestionMark")
        else
            icon:SetImage("Interface\\TargetingFrame\\UI-RaidTargetingIcon_" .. (9 - col))
        end
        icon:SetCallback("OnClick", function() sendColumn(col + 1) end)
        Icons[col] = icon
        self.treeGroup:AddChild(icon)
    end

    -- Create Roles label and mob editboxes
    RolesLabel = AceGUI:Create("Label")
    RolesLabel:SetFont("Fonts\\FRIZQT__.ttf", 16)
    RolesLabel:SetText("")
    self.treeGroup:AddChild(RolesLabel)

    for col = 1, MaxColumns do
        MobBoxes[col] = AceGUI:Create("EditBox")
        MobBoxes[col]:DisableButton(true)
        MobBoxes[col]:SetCallback("OnTextChanged", editBoxTextChanged)
        MobBoxes[col].col = col +1
        MobBoxes[col].row = 1
        self.treeGroup:AddChild(MobBoxes[col])
    end

    -- For each name row, create a send button, a role box, and name boxes
    for row = 1, MaxRows do
        local sendButton = AceGUI:Create("Button")
        sendButton:SetText(">")
        sendButton:SetCallback("OnClick", function() sendRow(row + 1) end)
        SendBoxes[row] = sendButton
        self.treeGroup:AddChild(sendButton)

        local roleBox = AceGUI:Create("EditBox")
        roleBox:DisableButton(true)
        roleBox:SetCallback("OnTextChanged", roleBoxTextChanged)
        -- roleBox:SetCallback("OnLeave", roleBoxTextChanged)
        roleBox.row = row + 1
        RoleBoxes[row] = roleBox

        self.treeGroup:AddChild(roleBox)

        NameBoxes[row] = {}
        for col = 1, MaxColumns do
            local nameBox = AceGUI:Create("Dropdown")
            -- nameBox:DisableButton(true)
            -- nameBox:SetCallback("OnTextChanged", editBoxTextChanged)
            nameBox:SetCallback("OnValueChanged", dropDownTextChanged)
            nameBox.GetText = function(b)
                return b.value and b.choices[b.value] or b.userdata.value
            end
            nameBox.row = row + 1
            nameBox.col = col + 1
            NameBoxes[row][col] = nameBox
            self.treeGroup:AddChild(nameBox)
        end
    end

    -- Create tactics send box and multibox
    local TacticsSendBox = AceGUI:Create("Button")
    TacticsSendBox:SetText(">")
    TacticsSendBox:SetCallback("OnClick", function()
        local text = TacticsBox:GetText()
        if strValid(text) then
            sendRaidWarning("TACTICS IN RAID CHAT")
            sendMessage(text)
        end
    end)
    self.treeGroup:AddChild(TacticsSendBox)

    TacticsBox = AceGUI:Create("MultiLineEditBox")
    TacticsBox:SetLabel("Tactics")
    TacticsBox:SetNumLines(5)
    TacticsBox:DisableButton(true)
    TacticsBox:SetCallback("OnTextChanged", tacticsTextChanged)
    self.treeGroup:AddChild(TacticsBox)

    -- Create sync button
    local SyncButton = AceGUI:Create("Button")
    SyncButton:SetText("SynAssign")
    SyncButton:SetCallback("OnClick", function() self:SendSync(SASSDB.last_page) end)
    self.treeGroup:AddChild(SyncButton)

    local NoteButton = AceGUI:Create("Button")
    NoteButton:SetText("Exorsus")
    NoteButton:SetCallback("OnClick", function() self:SyncExorsus() end)
    self.treeGroup:AddChild(NoteButton)

    local ChatButton = AceGUI:Create("Button")
    ChatButton:SetText("Chat")
    ChatButton:SetCallback("OnClick", function() self:SyncChat() end)
    self.treeGroup:AddChild(ChatButton)

    local CopyButton = AceGUI:Create("Button")
    CopyButton:SetText("Copy")
    CopyButton:SetCallback("OnClick", function()
        synassign.COPIED_PAGE = SASSDB.last_page
        cleanPage()
        populatePage(getSelectedPage())
    end)
    self.treeGroup:AddChild(CopyButton)

    local MarkButton = AceGUI:Create("Button")
    MarkButton:SetText("Mark")
    MarkButton:SetCallback("OnClick", function()
        synassign:MarkTargets()
    end)
    self.treeGroup:AddChild(MarkButton)

    PasteButton = AceGUI:Create("Button")
    PasteButton:SetText("Paste")
    PasteButton:SetCallback("OnClick", function() self:PasteValues() end)
    self.treeGroup:AddChild(PasteButton)

    local SyncLabel = AceGUI:Create("Label")
    SyncLabel:SetFont("Fonts\\FRIZQT__.ttf", 12)
    SyncLabel:SetText("Sync to:")
    SyncLabel:SetWidth(SyncLabel.label:GetUnboundedStringWidth())
    self.treeGroup:AddChild(SyncLabel)

    local AssignLabel = AceGUI:Create("Label")
    AssignLabel:SetFont("Fonts\\FRIZQT__.ttf", 12)
    AssignLabel:SetText("Assignments:")
    AssignLabel:SetWidth(AssignLabel.label:GetUnboundedStringWidth())
    self.treeGroup:AddChild(AssignLabel)

    -- Build GUI Layout
    AceGUI:RegisterLayout("EncounterLayout", function()
        if self.f.frame:GetWidth() < 747 then
            self.f:SetWidth(747)
        end
        if self.f.frame:GetHeight() < 430 then
            self.f:SetHeight(430)
        end

        local boxWidth = (self.treeGroup.content:GetWidth() - SendBoxes[1].frame:GetWidth()) / (MaxColumns +1)

        RoleBoxes[1]:ClearAllPoints()
        RoleBoxes[1]:SetWidth(boxWidth)
        RoleBoxes[1]:SetHeight(24)
        RoleBoxes[1]:SetPoint("LEFT", self.treeGroup.content, "TOPLEFT", 0, 0)
        for row = 2, MaxRows do
            RoleBoxes[row]:ClearAllPoints()
            RoleBoxes[row]:SetWidth(RoleBoxes[row - 1].frame:GetWidth())
            RoleBoxes[row]:SetHeight(RoleBoxes[row - 1].frame:GetHeight())
            RoleBoxes[row]:SetPoint("TOPLEFT", RoleBoxes[row - 1].frame, "BOTTOMLEFT", 0, 0)
        end

        Icons[1]:ClearAllPoints()
        Icons[1]:SetWidth(boxWidth)
        Icons[1]:SetPoint("BOTTOMLEFT", RoleBoxes[1].frame, "TOPRIGHT", 0, 24)
        for col = 2, MaxColumns do
            Icons[col]:ClearAllPoints()
            Icons[col]:SetWidth(Icons[col - 1].frame:GetWidth())
            Icons[col]:SetPoint("TOPLEFT", Icons[col - 1].frame, "TOPRIGHT", 0, 0)
        end

        MobBoxes[1]:ClearAllPoints()
        MobBoxes[1]:SetWidth(Icons[1].frame:GetWidth())
        MobBoxes[1]:SetHeight(24)
        MobBoxes[1]:SetPoint("TOPLEFT", Icons[1].frame, "BOTTOMLEFT", 0, 0)
        for col = 2, MaxColumns do
            MobBoxes[col]:ClearAllPoints()
            MobBoxes[col]:SetWidth(Icons[col].frame:GetWidth())
            MobBoxes[col]:SetHeight(MobBoxes[col - 1].frame:GetHeight())
            MobBoxes[col]:SetPoint("TOPLEFT", MobBoxes[col - 1].frame, "TOPRIGHT", 0, 0)
        end

        for row = 1, MaxRows do
            for col = 1, MaxColumns do
                NameBoxes[row][col]:ClearAllPoints()
                NameBoxes[row][col]:SetWidth(MobBoxes[col].frame:GetWidth())
                NameBoxes[row][col]:SetHeight(MobBoxes[col].frame:GetHeight())
                if row > 1 then
                    NameBoxes[row][col]:SetPoint("TOPLEFT", NameBoxes[row - 1][col].frame, "BOTTOMLEFT", 0, 0)
                else
                    NameBoxes[row][col]:SetPoint("TOPLEFT", MobBoxes[col].frame, "BOTTOMLEFT", 0, 0)
                end
            end
        end

        for row = 1, MaxRows do
            SendBoxes[row]:ClearAllPoints()
            SendBoxes[row]:SetWidth(16)
            SendBoxes[row]:SetPoint("LEFT", NameBoxes[row][MaxColumns].frame, "RIGHT", 0, 0)
        end

        TacticsSendBox:ClearAllPoints()
        TacticsSendBox:SetWidth(16)
        TacticsSendBox:SetHeight(MobBoxes[1].frame:GetHeight())
        TacticsSendBox:SetPoint("TOPRIGHT", SendBoxes[MaxRows].frame, "BOTTOMRIGHT", 0, -40)

        TacticsBox:ClearAllPoints()
        TacticsBox:SetWidth(self.treeGroup.content:GetWidth() - TacticsSendBox.frame:GetWidth())
        TacticsBox:SetPoint("RIGHT", TacticsSendBox.frame, "LEFT", 0, 0)

        SyncButton:ClearAllPoints()
        SyncButton:SetWidth(boxWidth)
        SyncButton:SetHeight(23)
        SyncButton:SetPoint("BOTTOMRIGHT", self.treeGroup.content, "BOTTOMRIGHT", 0, 0)

        NoteButton:ClearAllPoints()
        NoteButton:SetWidth(boxWidth)
        NoteButton:SetHeight(23)
        NoteButton:SetPoint("RIGHT", SyncButton.frame, "LEFT", 0, 0)

        ChatButton:ClearAllPoints()
        ChatButton:SetWidth(boxWidth)
        ChatButton:SetHeight(23)
        ChatButton:SetPoint("RIGHT", NoteButton.frame, "LEFT", 0, 0)

        SyncLabel:ClearAllPoints()
        SyncLabel:SetHeight(23)
        SyncLabel:SetPoint("RIGHT", ChatButton.frame, "LEFT", 0, 0)

        PasteButton:ClearAllPoints()
        PasteButton:SetWidth(boxWidth)
        PasteButton:SetHeight(23)
        PasteButton:SetPoint("BOTTOMRIGHT", SyncButton.frame, "TOPRIGHT", 0, 0)

        CopyButton:ClearAllPoints()
        CopyButton:SetWidth(boxWidth)
        CopyButton:SetHeight(23)
        CopyButton:SetPoint("RIGHT", PasteButton.frame, "LEFT", 0, 0)


        MarkButton:ClearAllPoints()
        MarkButton:SetWidth(boxWidth)
        MarkButton:SetHeight(23)
        MarkButton:SetPoint("RIGHT", CopyButton.frame, "LEFT", 0, 0)

        AssignLabel:ClearAllPoints()
        AssignLabel:SetHeight(23)
        AssignLabel:SetPoint("RIGHT", MarkButton.frame, "LEFT", 0, 0)

        RoleBoxes[1]:SetPoint("LEFT", self.treeGroup.content, "TOPLEFT", 0, -24*2.75)
    end)
    self.treeGroup:SetLayout("EncounterLayout")
    self.f:AddChild(self.treeGroup)
end

function synassign:RAID_ROSTER_UPDATE()
    if self.f:IsVisible() then
        cleanPage()
        populatePage(getSelectedPage())
    end
end

function synassign:OnEnable()
    synassign:PopulateInitialData()

    ChatFrame_AddMessageEventFilter("CHAT_MSG_PARTY", ChatHook)
    ChatFrame_AddMessageEventFilter("CHAT_MSG_RAID", ChatHook)
    ChatFrame_AddMessageEventFilter("CHAT_MSG_RAID_LEADER", ChatHook)
    ChatFrame_AddMessageEventFilter("CHAT_MSG_WHISPER", ChatHook)

    self:RegisterComm(synassign.COMM_PREFIX)
    self:RegisterEvent('RAID_ROSTER_UPDATE')

    synassign:BuildUI()
end

SLASH_SYNASSIGN1 = "/sa"
SLASH_SYNASSIGN2 = "/synassign"
SLASH_SYNASSIGN3 = "/syna"
SLASH_SYNASSIGN4 = "/sass"
SLASH_SYNASSIGN5 = "/sas"

SlashCmdList.SYNASSIGN = function(input)
    -- local bits = strTok(input:lower(), ' ')
    synassign:Toggle()
end
