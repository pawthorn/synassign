# SynAssign

Build up notes for your raid and easily change any assignments on the fly. Never let an assignment fall through the cracks again.

Open the UI via a minimap button (skull), libdatabroker or slash commands ( /sa /sass /syna /synassign )

As shown in images, you can create a page per boss, trash pack or however you feel. This is done by right clicking in the left area and selecting "Create"

Each page has rows where you can set the role of assignments (tank, heals, etc.) and these assignments will fill the subsequent dropdowns based on who is in your raid at that time.
For example: if tank is the role, the dropdowns will be populated based on the maintanks in the raid, see below for more roles that can be filled.

The dropdowns also show "???" if the previously selected person is not in the raid, so that you never miss an assignment.

Once you have the assignments you can print them out to chat by: clicking an icon for that column, the "&gt;" for the row or Sync to Chat at the bottom.
You can also sync the page over to exorsus notes via that button or to other people with SynAssign with that button.

You can also copy and paste assignments, when you press paste it will copy from the selected page each row of assignments where the roles match (tank/heal/etc.). We use this to quickly fill out tank assignments on all the pages for that raid.

Role based dropdowns (as long as the role starts with one of the below it will filter the dropdowns of that row):

- tank: main tanks
- maintank: main tanks
- assist: raid assistants
- mark: raid assistants
- groups: raid assistants
- heal: druid, priest, paladin, shaman
- druid: druid
- hunter: hunter
- mage: mage
- paladin: paladin
- priest: priest
- rogue: rogue
- shaman: shaman
- warlock: warlock
- warrior: warrior
- sleep: druid
- hots: druid, priest
- trap: hunter
- pack: hunter
- kite: hunter, mage
- poly: mage
- sheep: mage
- shackle: priest
- mc: priest
- control: priest
- stun: rogue, paladin
- kick: rogue, warrior, mage
- interrupt: rogue, warrior, mage
- tremor: shaman, priest
- ward: shaman, priest
- fearward: shaman, priest
- fear ward: shaman, priest
- fear: priest, warlock, warrior
- howl: warrior
- decurse: druid, mage
- disease: paladin, priest, shaman
- dedisease: paladin, priest, shaman
- dispel: paladin, priest
- tranq: hunter
- poison: shaman, paladin, druid
- depoison: shaman, paladin, druid

originally based on cleanraiders https://github.com/fjaros/cleanraiders
